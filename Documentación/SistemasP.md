## Sistemas P (P systems)

Dispositivos distrbuidos en paralelo.

### Definición:

Π = (O,μ,w1,...,wm,R1,...,Rm,i0)

- O : alfabeto que define objetos
- μ : define estructura de membranas : número de regiones : Grado de Π
- wi,1≤i≤m : cadenas de multiconjuntos sobre O
- Ri,1≤i≤m : Reglas de evolución sobre O
- i0 ∈ {1,2,...m} : Membranas de salida

(Ver referencia [9], página 16)

Sistema P --> Modelo computacional
**Ingrediente esencial:** Su estructura de membrana.

### Elementos:

[Estructura de membrana][membrana] con:
- Objetos en sus membranas
- Reglas especificas de evolución para los objetos
(Aplicadas de manera no determinista y paralela)
- Prescripciones de entrada-salida

[membrana]: ./Membrana.md

**Sistema no-coperativo:** Los objetos evolucionan solos

**Sistema coperativo:** Hay reglas que especifican la evolución de varios objetos en el mismo tiempo.

Caso particular: Sistemas catalíticos

**Sistemas de transición P**

### Características de los sistemas P

- Paralelismo intrinseco

### Variantes de sistemas P

Que trabajan a modo de:

a) Células
b) Tejidos
c) Neuronas

**Con membranas activas:** Aumentan eficiencia de cómputo