- Leer el capítulo 2 de la tesis que esta en Google Drive

- Averiguar si existen Sistemas P invertibles y si hay alguno implementado

- Buscar ejemplos de codificación, dónde se vea cómo llegar a la salida y cómo llegar a la contraria

- Averiguar en un modelo de criptografía con ADN cómo se devuelve el proceso de encriptación

Disponible en: 
https://pdfs.semanticscholar.org/9812/0136682c9181b3f85025e201db418336791b.pdf

- Buscar desarrollos de Sistemas P ya implementados para criptografía

Fuentes:

http://www.inf.ed.ac.uk/teaching/courses/nat/slides/nat17.pdf

Ideas

https://www.exabyteinformatica.com/uoc/Informatica/Criptografia_avanzada/Criptografia_avanzada_(Modulo_4).pdf