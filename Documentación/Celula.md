## Célula

### En biología:

Elemento más pequeño considerado vivo

Tipos de células:

- Eucariota:
En estas hay un núcleo que protege el material genetico, rodeado por una doble membrana. Tienen varias membranas

Ejemplos:
    - Neuronas: cuenta con 4 partes: el cuerpo, el axón, las sinapsis y las dendritas

- Procariota:
Propias de los organismos unicelulares, carece de un núcleo bien definido

Partes de una célula:

- Membrana plásmica: Especie de piel
- Núcleo: Almacena información genetica
- Citoplasma

## Super-célula:

Estructura de membrana con ciertos objetos ubicados en las regiones delimitadas por las membranas.

> _"Si los objetos de una super célula son capaces de evolucionar, entonces obtenemos un dispositivo informático, llamado [Sistemas P][sistemasP]"_ - (Referencia [4])

[sistemasP]: ./SistemasP.md