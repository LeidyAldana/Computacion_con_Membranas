> _"Cualquier sistema biologico no trivial es una construcción jerarquica donde un flujo intrincado de materiales e información toma lugar y el cual puede ser interpretado como un proceso computacional."_ - (Referencia [4])

## Computación con Membranas (Membrane computing)

La computación celular con membranas es considerada un paradigma de computación.

También es considerada una rama de la computación natural (o Computación molecular).

Área de las ciencias de la computación, que trata con modelos de computación paralelos y distribuidos.

### Computación natural:

- Redes neuronales (Nivel neuronal) [1943]
- Algoritmos geneticos (Nivel genetico) => Computación evolutiva [1975], inspirado en móleculas de ADN
- Computación con ADN (Nivel genetico) => Computación molecular [1944], inspirado en móleculas de ADN
- Computación con Membranas (Nivel Celular) => Gh. Paun [1998], extensión de la computación con ADN

### Analisis de célula con máquina, tomada de la referencia 1.

![Célula Máquina](../Imagenes/001.png "Tomada de la referencia 1")

[Célula][celula]: Unidad básica de todo organismo vivo,Permite ejecución de reacciones químicas
[celula]: ./Celula.md


Sistemas de membranas reciben el nombre de [Sistemas P][sistemasP]

[sistemasP]: ./SistemasP.md

### Otros apuntes

Definir modelo de computación => Formalizar concepto de procedimiento mecánico.