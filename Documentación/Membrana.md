## Estructura de membrana

### En computación: 

Contraparte matemática de arquitecturas jerarquicas compuesta de membranas.

Separador de 2 regiones (del espacio Euclidiano), con la posibilidad de comunicación selectiva.

Representación: Diagrama de Venn.

(La misma que se usa en la máquina química abstracta -[Chemical Abstract Machine][cabm]-)

[cabm]: ./CHAM.md

Se implementa un paralelismo masivo en dos niveles:

- Cada membrana aplica reglas en paralelo sobre sus objetos
- Todas las membranas realizan esta operación en paralelo

### En biología:

Función: Definir compartimientos

Elemento delimitador, estructura jerárquica

![Jerarquía](../Imagenes/002.png "Tomada de la referencia 10")

Tipos de membranas:
- Membrana más exterior (piel), como la 1
- Membranas elementales (No alojan en su interior nuevas membranas), también llamadas primitivas, como la 3, 4, 5
- Membranas no elementales (Albergan en su interior otras membranas)

Preserva los objetos en ella encerrados del efecto de otros elementos externos.