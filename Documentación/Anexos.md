## Software

**Membrane js:** Librería en JavaScript, disponible en: [https://github.com/davidlukerice/membranejs][membranejs]

[membranejs]: https://github.com/davidlukerice/membranejs



**P-Lingua:** Disponible en: [http://www.p-lingua.org/wiki/index.php/Main_Page][plingua]

[plingua]: http://www.p-lingua.org/wiki/index.php/Main_Page

## Aplicaciones

- Problemas de decisión

- Sistemas P como modelo de especificación formal para sistemas distribuidos

- Sistemas P como modelos para especificar y simular procesos biológicos