## Chemical Abstract Machine

Tema: Lenguajes de descripción arquitectonica (permiten mostrar la estructura arquitectonica)

Basado en: Analogía de soluciones químicas como arquitectura de Software

Cada estado es una solución química (hay reglas de reacción). Es especificada por la definición de moléculas y soluciones.

Las membranas son usadas para encerrar soluciones, pueden ser impermeables o semi-impermeables

Arquitectura --> Componentes y conectores

**Operadores o leyes:**

- Airlock: Permite extraer o agregar una molécula específica de una solución.