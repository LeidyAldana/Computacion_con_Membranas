# Computación con Membranas

Este repositorio contiene información en español tomada de diferentes referencias 
(contenidas en el archivo [Referencias.md][referencias]) acerca del tema Computación con Membranas.

[referencias]: ./Referencias.md

Toda la información esta contenida en formato Markdown por practicidad de lectura en el repositorio.

Para empezar, puede visualizar el archivo [Inicio.md][inicio]

Autor: Leidy Marcela Aldana, (LeidyMarcelaAldana@gmail.com)

Los temas que contiene la documentación en este repositorio son:

1. [Inicio][inicio]

[inicio]: ./Documentacion/Inicio.md

2. [Sistemas P][sistemasP]

[sistemasP]: ./Documentacion/SistemasP.md

3. [Estructura de Membrana][membrana]

[membrana]: ./Documentacion/Membrana.md

4. [Célula][celula]

[celula]: ./Documentacion/Celula.md

5. [Anexos][anexos]

[anexos]: ./Documentacion/Anexos.md