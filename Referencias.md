## Referencias

[1]. Las matemáticas de la vida. P systems by example. _Conceptos básicos de la computación celular con membranas_, disponible en: [http://www.p-lingua.org/mecosim/matvida/course/Unidad2_Computaci%F3nCelularConMembranas.pdf][blog]

[blog]: http://www.p-lingua.org/mecosim/matvida/course/Unidad2_Computaci%F3nCelularConMembranas.pdf

Artículos escritos por _Gheorghe Paun_:

[2]. Gheorghe Paun, _A quick introduction to membrane computing_. (2010) Sevilla, Spain. Elsevier. doi:10.1016/j.jlap.2010.04.002

[3]. P Systems Webpage, disponible en: [http://ppage.psystems.eu/][pagPsystems]

[pagPsystems]: http://ppage.psystems.eu/

[4]. Gheorghe Paun, _Computing with Membranes_. (2000) Romania. https://doi.org/10.1006/jcss.1999.1693

[5]. Gheorghe Paun, Grzegorz Rozenberg _A guide to membrane computing_. (2002) Spain. Elsevier. Disponible en: [http://profs.scienze.univr.it/~manca/mnc/membrane.pdf][agtmc]

[agtmc]: http://profs.scienze.univr.it/~manca/mnc/membrane.pdf

[6]. Gheorghe Paun, _Chapter 1. Introduction to membrane computing_. Sevilla, Spain. Disponible en: [http://cs.ioc.ee/yik/schools/win2007/paun/IntrodMC.pdf][itmc]

[itmc]: http://cs.ioc.ee/yik/schools/win2007/paun/IntrodMC.pdf

[7]. Gheorghe Paun, _Computing with Cells and Atoms: After Five Years_. (2004) Sevilla, Spain. Disponible en: [https://pdfs.semanticscholar.org/f5b7/a3dcc8ab30cf214e86d8ba95f1432cc683f9.pdf][cwcaaafy]

[cwcaaafy]: https://pdfs.semanticscholar.org/f5b7/a3dcc8ab30cf214e86d8ba95f1432cc683f9.pdf

Otros trabajos realizados por diferentes autores:

[8]. M.J. Pérez, A. Romero, F. Sancho, _Modelos de computación celular con membranas_. (2004) Sevilla. Disponible en: [https://idus.us.es/xmlui/bitstream/handle/11441/48581/191-220-1-PB.pdf?sequence=1&isAllowed=y][mdcccm]

[mdcccm]: https://idus.us.es/xmlui/bitstream/handle/11441/48581/191-220-1-PB.pdf?sequence=1&isAllowed=y

[9]: D. Pérez. _Computación celular con membranas: un estudio de modelos de cómputo bioinspirados_. Madrid. Disponible en: [http://oa.upm.es/39518/1/DAVID_PEREZ_PEREZ.pdf][cccmuedmdcb]

[cccmuedmdcb]: http://oa.upm.es/39518/1/DAVID_PEREZ_PEREZ.pdf

[10]. Góngora P. _Computación basada en membranas_. (2007) Disponible en: [http://turing.iimas.unam.mx/~pedroG/membranas.pdf][gcbem]

[gcbem]: http://turing.iimas.unam.mx/~pedroG/membranas.pdf

Libros:

[11].  Pierluigi Frisco, _Computing with Cells: Advances in Membrane Computing_. (2009) Oxford , consultado en: [https://books.google.com.co/books?hl=es&lr=&id=HuGNiMkE9s4C&oi=fnd&pg=PT3&dq=Frisco,+P.:+Computing+with+Cells.+Advances+in+Membrane+Computing.+Oxford+Univ.Press,+Oxford&ots=guMuD01_Xm&sig=10KyiktuTE5Phc_JY4DwBFrPS4s#v=onepage&q&f=false][cwcaimc]

[cwcaimc]: https://books.google.com.co/books?hl=es&lr=&id=HuGNiMkE9s4C&oi=fnd&pg=PT3&dq=Frisco,+P.:+Computing+with+Cells.+Advances+in+Membrane+Computing.+Oxford+Univ.Press,+Oxford&ots=guMuD01_Xm&sig=10KyiktuTE5Phc_JY4DwBFrPS4s#v=onepage&q&f=false

Referencias utilizadas acerca de otros temas:

[12]. Sintaxis Markdown, consultado en: [https://markdown.es/sintaxis-markdown/][md]

[md]: https://markdown.es/sintaxis-markdown/